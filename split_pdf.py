from PyPDF2 import PdfFileReader, PdfFileWriter


def get_num_pages(base_name):
    with open('{}.pdf'.format(base_name), 'rb') as input_pdfstream:
        inputpdf = PdfFileReader(input_pdfstream)

        return inputpdf.numPages


def calculate_page_saturations(base_name, total_pages):
    import subprocess
    print("Calculating max saturations for {} pages from {}.pdf (may take a while)".format(
        total_pages, base_name))

    return subprocess.check_output([
        './calculate_pages_saturations.sh',
        '{}.pdf'.format(base_name),
        str(total_pages)
    ], universal_newlines=True)


def filter_pages_by_threshold(tagged_page_stream, threshold):
    positive_cases = []
    for line in tagged_page_stream.split('\n')[:-1]:
        print(line)
        page_num, max_sat = line.split(', ')
        positive_cases.append((page_num, float(max_sat.strip())))

    return [int(page) for page, value in positive_cases if value > threshold]


def split_by_page_list(page_list, max_page):
    from itertools import chain
    page_set = set(page_list)

    color_page_pairs = chain(*[(page, page + 1)
                               for page in range(1, max_page, 2)
                               if page in page_set or page + 1 in page_set])
    bw_page_pairs = chain(*[(page, page + 1)
                            for page in range(1, max_page, 2)
                            if page in page_set or page + 1 not in page_set])

    return color_page_pairs, bw_page_pairs


def split_pdf(input_stream, file_pages):
    inputpdf = PdfFileReader(input_stream)

    for output_stream, page_list in file_pages:
        output_pdf = PdfFileWriter()
        for page in page_list:
            output_pdf.addPage(inputpdf.getPage(page))

        output_pdf.write(output_stream)


def main(base_name, saturation_threshold):
    num_pages = get_num_pages(base_name)

    page_saturations = calculate_page_saturations(base_name, num_pages)
    filtered_pages = filter_pages_by_threshold(page_saturations, saturation_threshold)

    color_list, bw_list = split_by_page_list(filtered_pages, num_pages)

    with open('{}_color.pdf'.format(base_name), 'wb') as output_file_color:
        with open('{}_bw.pdf'.format(base_name), 'wb') as output_file_bw:
            with open('{}.pdf'.format(base_name), 'rb') as input_pdf:
                split_pdf(input_pdf, [(output_file_color, color_list), (output_file_bw, bw_list)])


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input', help="PDF file to extract pages from")
    parser.add_argument('-s', '--saturation',
                        type=float,
                        help="Saturation threshold to split pages that are 'color' vs 'grayscale'",
                        default=30000)

    args = parser.parse_args()
    base_name, _ = args.input.split('.')

    main(base_name, args.saturation)

