# README #

This is a very simple script that does the following:

* Reads an input pdf file
* Analyses each page and decides if the page is a color page
* Outputs a pdf file with only grey scale pages
* Outputs a pdf file with color pages and possibly some grey scale pages

Output files are supposed to be printed two sided. For this reason, not all pages on the color pdf file will be color.

### Set up ###

Install [imagemagick](https://www.imagemagick.org/script/binary-releases.php)

`pip install -r requirements.txt`

### Running ###

`python split_pdf.py file.pdf [-s <saturation threshold>]` 

### How are color pages detected? ###

After trying several things, I found out the simplest and most reliable way to do this is using imagemagick convert to fetch stats on each page HSV channel. The script will look at the saturation channel statistics assume pages with a maximum saturation above a threshold are color, while pages with a maximum saturation below that threshold are assumed to be grayscale.

Default saturation threshold is 300000 (which is about half way from the maximum 65536 for 16 bits channels). A saturation of 128 can be used for 8 bits channels.

### TODO ###

* Allow configuring the assumption that the first page is the book cover
* Allow configuring the assumption that the outputs will be printed two sided
* Filter by normalised threshold so that saturation threshold is 0.5 for 8 and 16 bit channels instead of 30000 or 128
* Automatic threshold selection?