#!/bin/bash
FILE=$1
TOTAL_PAGES=$2

for i in $(seq 0 ${TOTAL_PAGES}); do
    COLOR=$(convert ${FILE}[${i}]  -colorspace HSV -verbose info: | grep -A 2 'Channel 1:$' | sed -n 's/      max: \([0-9]\{1,5\}.\{0,1\}[0-9]\{0,4\}\) (.*)/\1/p')
    
    echo $i, $COLOR
    >&2 printf "${i} "
done